resource "aws_instance" "my_instance" {
  ami           = "${var.ec2_ami}"
  instance_type = "${var.instanceType}"
  key_name = "${var.instance_key}"
  vpc_security_group_ids = [ aws_security_group.allow_ssh.id ]
#  subnet_id = aws_subnet.terraform_subnet.id
  associate_public_ip_address = true

  tags = {
    Name = "var.instance_Name"
  }
}

